**Q Puzzle Game**

The game contain a Play form and a Design form.

---

## Run the App

You�ll start the app in visual studio 2019

1. Download Visual studio 2019.
2. Click the Run button in Visual Studio 2019.
3. Wait the App successfully running.

---

---

## Create/Design a Puzzle(Design form)

This is the first function of this app

1. Click the Run button in Visual Studio 2019
2. Click the **Design** button in the running app
3. Click the Wall, Box, Door button to design the game.
4. Click on **File** button and choose **Save**.
5. Enter the name for your designed game file.
6. Click on **Close** to exit the form

---

## Play the Game(Play form)

Next, you can play the game base on the designed puzzle

1. Click the **Play** button in the running app
2. Click the **File** button and then click *Load game* to choose the saved game file in the file explorer
3. Click on **Up** ,**Down**, **Left**, **Right** to play the game
4. Click on **Close** to exit the form

---

## Team EPJ Public License

> Version 1.1, FEBRUARY 2021

> Copyright (C) [2021] [Elaine Sun, Peng Yu, Junbeom Kim]

> THIS IS AN ASSIGNMENT #3 FOR COURSE - PROG2370.

>TERM OF COPY, DISTRIBUTE OR MODIFY

Allowing viewers to modify, copy, or distribute this code. However, the permission is not unlimited for following situations:
 
1. Completely copying
2. For any kind of business purposes
3. Harmfully, unlawfully or any inappropriate changing  

Thanks

---

## Explainations for the choice of license

1. Completely copying
Fully copying and then only changing the author name is not a good thing. Also, since this is a College assignment, students can copy it and submit it too. In my opinion, this is not good for studying.

2. For any kind of business purposes
Considering many of the code authors, they are very diligent, smart and nice so they share their ideas and codes which both could be very helpful for other programmers. If somebody takes advantage of people�s kindness to make money. It could be very sad and destroy the passion of sharing and helping. 

3. Harmfully, unlawfully or any inappropriate changing  
In case some bad guys can just add some harmful code in this project and spread it out to ransom, blackmail etc. They are not allowed to use them.

---

**a REVERT which works for question 5** 